# dependabot-standalone

Pipeline configuration that runs [dependabot-gitlab](https://gitlab.com/dependabot-gitlab/dependabot) for dependency
updates via gitlab pipelines without deployment.

## Configuration

Target project for updates will require valid [.gitlab/dependabot.yml](https://docs.github.com/en/github/administering-a-repository/configuration-options-for-dependency-updates) file.

### Pipeline configuration

### Fork

- Fork the repository and setup scheduled jobs in the fork

### Manual

- Copy job definitions from [.gitlab-ci.yml](.gitlab-ci.yml) in to your own `.gitlab-ci.yml` file.

### Include

- Include pipeline template directly:

```yml
include:
  - project: "dependabot-gitlab/dependabot-standalone"
    file: ".gitlab-ci.yml"
```

- Optionally override stage where dependabot jobs are executed (`test` stage by default)

```yml
stages:
  - ...existing stages...
  - dependabot

.dependabot-gitlab:
  stage: dependabot
```

### Settings => CI/CD => Variables

Add following variables to be available in pipelines:

- `SETTINGS__GITLAB_ACCESS_TOKEN` - gitlab personal access token with `api` access scope and at least `Developer` role
- `SETTINGS__GITHUB_ACCESS_TOKEN` - github personal access token with repository read scope

For a full list of environment variables supported, refer to [environment.md](https://gitlab.com/dependabot-gitlab/dependabot/-/blob/main/doc/environment.md) configuration section.

### CI/CD => Schedules

Create scheduled job with following environment variables:

- `PROJECT_PATH` - project path, like `dependabot-gitlab/dependabot`
- `PACKAGE_MANAGER_SET` - comma separated package eco-systems, like `bundler,docker` (dependency file must be in the same directory for multiple package managers to work within same schedule job)
- `DIRECTORY` - update directory, usually `/` if not a larger monorepo

Combination of `PACKAGE_MANAGER_SET` and `DIRECTORY` must exist in `dependabot.yml` file, these values do not override configuration, but act as a pointer for specific entry in `dependabot.yml` file.
